﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppCSUSM
{
    public partial class MainPage : ContentPage
    {
            int state = 1;

        public MainPage()
        {
            InitializeComponent();
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            if (state == 1)
            {
                resultText.Text = "";
                state = 0;
            }
            resultText.Text += (sender as Button).Text;
        }
        void Handle_Clicked_Clear(object sender, System.EventArgs e)
        {
            if (state == 0)
            {
                resultText.Text = "0";
                state = 1;
            }
        }
        void Handle_Clicked_Result(object sender, System.EventArgs e)
        {
            resultText.Text = "result";
        }
    }
}
