﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Midterm
{
    public partial class ListPage : ContentPage
    {
        public static Dictionary<string, TimeSeriesDaily> stockData;
        public static String NameSym;

        public ListPage()
        {
            InitializeComponent();
        }

        async void Handle_Clicked(object sender, System.EventArgs e)
        {
            var apiKey = "GILSSRF56N4LJAKP";
            var MyEntry = new Entry();
            var client = new HttpClient();
            var stockApiAdress = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=" + searchBar.Text + "&apikey=" + apiKey;

            var uri = new Uri(stockApiAdress);

            StockData stock = new StockData();

            var response = await client.GetAsync(uri);

            if (response.IsSuccessStatusCode)
            {
                var jsonContent = await response.Content.ReadAsStringAsync();
                stock = JsonConvert.DeserializeObject<StockData>(jsonContent);

                if (stock.TimeSeriesDaily == null)
                {
                    await DisplayAlert("Alert", "Wrong symbol", "OK");
                }
            }

            if (stock == null)
            {
                return;
            }

            stockData = stock.TimeSeriesDaily;
            ListStockJson.ItemsSource = stockData;

            GraphPage grp = new GraphPage();
            grp.CreateCharts(stockData, NameSym);

            var maxValue = 0.0;
            var minValue = 0.0;
            if (stockData != null)
            {
                foreach (KeyValuePair<string, TimeSeriesDaily> value in stockData)
                {
                    var convertedValue = Convert.ToDouble(value.Value.The2High);
                    var minConvertedValue = Convert.ToDouble(value.Value.The3Low);
                    minValue = Convert.ToDouble(value.Value.The3Low);
                    if (convertedValue > maxValue)
                    {
                        maxValue = convertedValue;
                    }
                    if (minValue < minConvertedValue)
                    {
                        minValue = minConvertedValue;
                    }
                }
            }
            if (stockData != null)
            {
                maxvalue.Text = "Highest : $" + maxValue.ToString();
                minvalue.Text = "Lowest : $" + minValue.ToString();
            }
            else
            {
                maxvalue.Text = "";
                minvalue.Text = "";
            }

            if (stock.MetaData == null)
                return;
            NameSym = stock.MetaData.The2Symbol;
        }
    }
}