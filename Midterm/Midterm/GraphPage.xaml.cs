﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Net.Http;
using System.Threading.Tasks;
using Microcharts;
using Newtonsoft.Json;
using SkiaSharp;
using Xamarin.Forms;
using Entry = Microcharts.Entry;

namespace Midterm
{
    public partial class GraphPage : ContentPage
    {

        public GraphPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            CreateCharts(ListPage.stockData, ListPage.NameSym);
        }

        public void CreateCharts(Dictionary<string, TimeSeriesDaily> stock1, string metaData)
        {
            if (stock1 == null)
                return;
            if (metaData == null)
                return;

            var entryList = new List<Entry>();

            metaData = ListPage.NameSym;
            symbol.Text = "Symbol : " + metaData;

            foreach (KeyValuePair<string, TimeSeriesDaily> value in stock1)
            {
                entryList.Add(new Entry(float.Parse(value.Value.The2High, CultureInfo.InvariantCulture.NumberFormat))
                {
                    ValueLabel = value.Value.The2High,
                    Color = SKColor.Parse("#005b96")
                });
            }

            var entries1 = new List<Entry>();
            for (int i = 0; i <= 99; i += 5)
            {
                entries1.Add(entryList[i]);
            }

            var entries2 = new List<Entry>();
            for (int i = 0; i <= 29; i += 2)
            {
                entries2.Add(entryList[i]);
            }

            var LineChart1 = new LineChart() { Entries = entries1 };
            Chart1.Chart = LineChart1;
            Chart1.Chart.LabelTextSize = 40;

            var LineChart2 = new LineChart() { Entries = entries2 };
            Chart2.Chart = LineChart2;
            Chart2.Chart.LabelTextSize = 40;
        }
    }
}