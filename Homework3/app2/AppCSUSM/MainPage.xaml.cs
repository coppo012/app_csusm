﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppCSUSM
{
    public partial class MainPage : MasterDetailPage
    {
        public MainPage()
        {
            InitializeComponent();
            Detail = new NavigationPage(new Page1());
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            Detail = new NavigationPage(new Page1());
            IsPresented = false;
        }

        void Handle_Clicked2(object sender, System.EventArgs e)
        {
            Detail = new NavigationPage(new Page2());
            IsPresented = false;
        }
        void Handle_Clicked3(object sender, System.EventArgs e)
        {
            Detail = new NavigationPage(new Page3());
            IsPresented = false;
        }
        void Handle_Clicked4(object sender, System.EventArgs e)
        {
            Detail = new NavigationPage(new Page4());
            IsPresented = false;
        }
        void Handle_Clicked5(object sender, System.EventArgs e)
        {
            Detail = new NavigationPage(new Page5());
            IsPresented = false;
        }
    }
}
