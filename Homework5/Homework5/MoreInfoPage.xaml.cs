﻿using System;
using System.Collections.Generic;
using Homework5.Models;
using Xamarin.Forms;

namespace Homework5
{
    public partial class MoreInfoPage : ContentPage
    {
        public MoreInfoPage()
        {
            InitializeComponent();
        }

        public MoreInfoPage(ProductsData productsData)
        {
            InitializeComponent();

            BindingContext = productsData;
        }
    }
}
