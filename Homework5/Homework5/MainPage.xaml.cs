﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Homework5.Models;
using Xamarin.Forms;

namespace Homework5
{
    public partial class MainPage : ContentPage
    {
        List<ProductsData> productsDataFromJson = new List<ProductsData>();

        public MainPage()
        {
            InitializeComponent();

            ReadInJsonFile();
        }

        private void ReadInJsonFile(var search)
        {
            var api_key = "GILSSRF56N4LJAKP";
            var dogApiAddress = "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=" + search + "&interval=5min&apikey=" + api_key;
            var uri = new Uri(dogApiAddress);


            var response = await client.GetAsync(uri);

            var fileName = ;

            var assembly = typeof(MainPage).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream(fileName);

            using (var reader = new System.IO.StreamReader(stream))
            {
                var jsonAsString = reader.ReadToEnd();
                productsDataFromJson = JsonConvert.DeserializeObject<List<ProductsData>>(jsonAsString);
            }
            ListProductsJson.ItemsSource = new ObservableCollection<ProductsData>(productsDataFromJson);
        }

        async void Handle_Clicked(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var itemClicked = menuItem.CommandParameter as ProductsData;
            await Navigation.PushAsync(new MoreInfoPage(itemClicked));
        }
    }
}
