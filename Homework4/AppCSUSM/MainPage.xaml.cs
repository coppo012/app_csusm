﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppCSUSM
{

    public partial class MainPage : ContentPage
    {

        public ObservableCollection<ImageCellItem> imageCellItems = new ObservableCollection<ImageCellItem>();

        public MainPage()
        {
            InitializeComponent();

            PrintList();
        }

        private void PrintList()
        {
            imageCellItems.Add(new ImageCellItem()
            {
                IconSource = ImageSource.FromFile("messi.png"),
                ImageText = "Messi",
                DetailText = "best foot : left",
                SkillText = "skills : *****",
                NationText = "Argentina",
            });
            imageCellItems.Add(new ImageCellItem()
            {
                IconSource = ImageSource.FromFile("zidane.png"),
                ImageText = "Zidane",
                DetailText = "best foot : right",
                SkillText = "skills : *****",
                NationText = "France",
            });
            imageCellItems.Add(new ImageCellItem()
            {
                IconSource = ImageSource.FromFile("ronaldinho.png"),
                ImageText = "Ronaldinho",
                DetailText = "best foot : right",
                SkillText = "skills : *****",
                NationText = "Brasil",
            });
            imageCellItems.Add(new ImageCellItem()
            {
                IconSource = ImageSource.FromFile("germain.png"),
                ImageText = "Germain",
                DetailText = "best foot : right",
                SkillText = "skills : *",
                NationText = "France",
            });
            imageCellItems.Add(new ImageCellItem()
            {
                IconSource = ImageSource.FromFile("beckham.png"),
                ImageText = "Beckham",
                DetailText = "best foot : right",
                SkillText = "skills : ****",
                NationText = "England",
            });

            ImageCellsListView.ItemsSource = imageCellItems;
        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            imageCellItems.Clear();
            PrintList();
            ImageCellsListView.IsRefreshing = false;
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var item = (ImageCellItem)menuItem.CommandParameter;
            imageCellItems.Remove(item);

        }
    }
}