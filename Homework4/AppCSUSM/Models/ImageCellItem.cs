﻿using System;
using Xamarin.Forms;

namespace AppCSUSM
{
    public class ImageCellItem
    {
        public string ImageText
        {
            get;
            set;
        }

        public ImageSource IconSource
        {
            get;
            set;
        }

        public string DetailText
        {
            get;
            set;
        }

        public string SkillText
        {
            get;
            set;
        }

        public string NationText
        {
            get;
            set;
        }
    }
}